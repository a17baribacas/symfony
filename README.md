# Symfony

El link del manual a seguir és:
[https://github.com/symfony/demo](https://github.com/symfony/demo)

Problemes que m'he trobat amb el manual:
1. Hem demana una extensió, **ext-pdo_sqlite**, la he instal·lat, em seguia donant error, ho he solucionat, actualitzant, a la versió **php7.0-sqlite**, i reinitziant el servidor apache.
2. Un altre gran problema o diferència que m'he trobat, és que le manual t'indica les instruccions de:
"symfony new --demo my_project"
"symfony serve"
"./bin/phpunit"
i aquestes instruccions no me les accepta, he utilitzat unes altres pròpies de symfony que he anant utilitzant durant el curs, com *"composer"*,* "php bin/console server:run"*, en ordre, tant per crear el projecte com per inicialitzar-lo al servidor.

Una vegade aixecat el servidor adjunto una **captura de pantalla**

[(https://bitbucket.org/a17baribacas/symfony/src/master/demo_barbara.png)](https://bitbucket.org/a17baribacas/symfony/src/master/demo_barbara.png)

El link de la web desplegada és:
[http://localhost:8000/ca](http://localhost:8000/ca)

Per últim he fet els canvis per el labs, al document root i demés, i als arxius de configuració de symfony, per indicar al directori de /web com públic, però no em surt la pàgina. Adjunto el link a labs.:
[http://labs.iam.cat/~a17baribacas/symfony/my_project/](http://labs.iam.cat/~a17baribacas/symfony/my_project/)
